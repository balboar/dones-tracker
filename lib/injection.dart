import 'package:get_it/get_it.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';
import 'package:seguidor/features/main_page/repositories/location_tracking_repo.dart';
import 'package:seguidor/features/settings_page/cubit/location_settings_cubit.dart';
import 'package:seguidor/features/sync/cubit/sync_cubit.dart';
import 'package:seguidor/features/upload_track_file/cubit/upload_track_file_cubit.dart';

final sl = GetIt.I; // sl == Service Locator

Future<void> init() async {
// ! application Layer
// ! domain Layer

  sl.registerLazySingleton<LocationTrackingRepo>(() => LocationTrackingRepo());
  // Factory = every time a new/fresh instance of that class
  sl.registerFactory<LocationTrackingCubit>(
      () => LocationTrackingCubit(locationTrackingRepo: sl()));
  sl.registerFactory<SyncCubit>(() => SyncCubit(locationTrackingRepo: sl()));
  sl.registerFactory<UploadTrackFileCubit>(
      () => UploadTrackFileCubit(locationTrackingRepo: sl()));

  // Register LocationSettingsCubit as a singleton
  sl.registerSingleton<LocationSettingsCubit>(LocationSettingsCubit());
}
