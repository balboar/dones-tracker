import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:seguidor/flavor_config.dart';
import 'package:seguidor/app.dart';
import 'injection.dart' as di;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!Platform.isLinux) {
    await Firebase.initializeApp();
  }
  await di.init();
  FlavorConfig(
    flavor: Flavor.dev,
    name: 'Seguidor DEV',
    color: Colors.deepPurpleAccent,
  );
  runApp(const MyApp());
}
