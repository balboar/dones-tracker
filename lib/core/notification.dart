import 'dart:convert';
import 'package:googleapis_auth/auth_io.dart' as auth;
import 'package:flutter/services.dart';

Future<bool> sendPushMessage({
  required String recipientToken,
  required String title,
  required String body,
}) async {
  final jsonCredentials =
      await rootBundle.loadString('lib/assets/firebase-sdk_keys.json');
  final creds = auth.ServiceAccountCredentials.fromJson(jsonCredentials);

  final client = await auth.clientViaServiceAccount(
    creds,
    ['https://www.googleapis.com/auth/cloud-platform'],
  );

  final notificationData = {
    'message': {
      'token': recipientToken,
      'notification': {'title': title, 'body': body}
    },
  };

  const String senderId = '531483963833';
  final response = await client.post(
    Uri.parse('https://fcm.googleapis.com/v1/projects/$senderId/messages:send'),
    headers: {
      'content-type': 'application/json',
    },
    body: jsonEncode(notificationData),
  );

  client.close();
  return response.statusCode == 200;
}
