import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:seguidor/features/main_page/widgets/request_location_permission_dialog.dart';

Future<void> requestLocationPermission(BuildContext context) async {
  await Permission.location.shouldShowRequestRationale.then((value) async {
    if (value) {
      await Permission.location.request();
    } else {
      await showDialog(
          context: context,
          builder: (_) => const RequestLocationPermissionDialog());
    }
  });
}

Future<void> requestLocationAlwaysPermission(BuildContext context) async {
  await Permission.locationAlways.shouldShowRequestRationale
      .then((value) async {
    if (value) {
      await Permission.locationAlways.request();
    } else {
      await showDialog(
          context: context,
          builder: (_) => const RequestLocationPermissionDialog());
    }
  });
}

Future<bool> handleLocationPermissionStatus(
    BuildContext context, PermissionStatus status) async {
  if (status case PermissionStatus.granted) {
    return Future.value(true);
  } else {
    await requestLocationPermission(context);
    return Future.value(false);
  }
}

Future<bool> handleLocationAlawaysPermissionStatus(
    BuildContext context, PermissionStatus status) async {
  if (status case PermissionStatus.granted) {
    return Future.value(true);
  } else {
    await requestLocationAlwaysPermission(context);
    return Future.value(false);
  }
}

Future<bool> isPermissionLocationsGranted(BuildContext context) async {
  return await Permission.location.status.then((value) async {
    return await handleLocationPermissionStatus(context, value);
  });
}

Future<bool> isPermissionLocationsAlwaysGranted(BuildContext context) async {
  bool locationGranted;
  locationGranted = await Permission.location.status.then((value) async {
    return await handleLocationPermissionStatus(context, value);
  });
  if (locationGranted) {
    return await Permission.locationAlways.status.then((value) async {
      return await handleLocationAlawaysPermissionStatus(context, value);
    });
  } else {
    return Future.value(false);
  }
}
