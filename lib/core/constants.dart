enum Sport {
  bike('Ciclismo', 'lib/assets/cycling.png', '🚴'),

  run('Correr', 'lib/assets/running.png', '🏃'),

  walk('Andar', 'lib/assets/walking.png', '🚶'),

  ebike('E-bike', 'lib/assets/e-bike.png', '🚴'),
  ;

  final String label;
  final String imagePath;
  final String icon;

  const Sport(this.label, this.imagePath, this.icon);
}
