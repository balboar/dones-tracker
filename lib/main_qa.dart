import 'package:flutter/material.dart';
import 'package:seguidor/flavor_config.dart';
import 'package:seguidor/app.dart';
import 'package:firebase_core/firebase_core.dart';

import 'injection.dart' as di;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await di.init();
  FlavorConfig(
    flavor: Flavor.qa,
    name: 'Seguidor QA',
    color: Colors.deepPurpleAccent,
  );
  runApp(const MyApp());
}
