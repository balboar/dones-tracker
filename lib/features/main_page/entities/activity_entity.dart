// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:math' as math;
import 'dart:core';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';
import 'package:latlong2/latlong.dart';

import 'package:seguidor/features/main_page/entities/gps_point_entity.dart';
import 'package:seguidor/core/constants.dart';

class Activity extends Equatable {
  final String uuid = const Uuid().v4();
  final DateTime date = DateTime.now();
  String? gpxRoute;
  bool _isFinished = false;
  final List<GpsPoint> _gpsPoints = [];
  final List<LatLng> _positions = [];
  final Sport sport;
  Activity({
    required this.sport,
  });

  List<LatLng> get positions => _positions;
  List<GpsPoint> get gpsPoints => _gpsPoints;

  double _computeHeading(LatLng source, LatLng destination) {
    double dLon = (destination.longitudeInRad - source.longitudeInRad);
    double y = math.sin(dLon) * math.cos(destination.latitudeInRad);
    double x =
        math.cos(source.latitudeInRad) * math.sin(destination.latitudeInRad) -
            math.sin(source.latitudeInRad) *
                math.cos(destination.latitudeInRad) *
                math.cos(dLon);
    double brng = math.atan2(y, x);
    brng = (brng * 180 / math.pi + 360) % 360;
    return brng;
  }

  void addPosition(GpsPoint position) {
    position.ruta = uuid;
    if (position.heading == 0 && _gpsPoints.isNotEmpty) {
      var lastPoint = _gpsPoints.last;
      var sourceLatLng = LatLng(lastPoint.latitude, lastPoint.longitude);
      var destLatLng = LatLng(position.latitude, position.longitude);
      position.heading = _computeHeading(sourceLatLng, destLatLng);
    }
    // if (position.accuracy < 10 || !kReleaseMode)
    {
      _gpsPoints.add(position);
      _positions.add(LatLng(position.latitude, position.longitude));
    }
  }

  void stop() {
    _isFinished = true;
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'uuid': uuid,
      'date': date.millisecondsSinceEpoch,
      'sport': sport.name,
      'gpxRoute': gpxRoute,
      'finished': _isFinished == true ? 1 : 0,
    };
  }

  String toJson() => json.encode(toMap());

  @override
  List<Object?> get props => [sport, uuid, date, gpxRoute, _isFinished];
}
