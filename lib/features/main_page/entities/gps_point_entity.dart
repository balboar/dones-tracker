import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';

class GpsPoint extends Equatable {
  final double latitude;
  final double longitude;
  final double accuracy;
  final double altitude;
  final double speed;
  final double speedAccuracy;
  double heading;
  final DateTime time;
  late final String ruta;
  late final int battery;
  bool sync = false;
  GpsPoint({
    required this.latitude,
    required this.longitude,
    required this.accuracy,
    required this.altitude,
    required this.speed,
    required this.speedAccuracy,
    required this.heading,
    required this.time,
  });

  GpsPoint copyWith({
    double? latitude,
    double? longitude,
    double? accuracy,
    double? altitude,
    double? speed,
    double? speedAccuracy,
    double? heading,
    DateTime? time,
  }) {
    return GpsPoint(
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      accuracy: accuracy ?? this.accuracy,
      altitude: altitude ?? this.altitude,
      speed: speed ?? this.speed,
      speedAccuracy: speedAccuracy ?? this.speedAccuracy,
      heading: heading ?? this.heading,
      time: time ?? this.time,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': 0,
      'latitude': latitude,
      'longitude': longitude,
      'accuracy': accuracy,
      'elevation': altitude,
      'speed': speed,
      'speed_accuracy': speedAccuracy,
      'heading': heading,
      'date': time.millisecondsSinceEpoch.toInt(),
      'ruta_id': ruta,
      'battery': battery,
      'elevation_original': altitude
    };
  }

  factory GpsPoint.fromLocationData(Position data) {
    return GpsPoint(
        latitude: data.latitude,
        longitude: data.longitude,
        accuracy: data.accuracy,
        altitude: data.altitude,
        speed: data.speed,
        speedAccuracy: data.speedAccuracy,
        heading: data.heading,
        time: data.timestamp);
  }

  factory GpsPoint.fromMap(Map<String, dynamic> map) {
    return GpsPoint(
      latitude: map['latitude']?.toDouble() ?? 0.0,
      longitude: map['longitude']?.toDouble() ?? 0.0,
      accuracy: map['accuracy']?.toDouble() ?? 0.0,
      altitude: map['altitude']?.toDouble() ?? 0.0,
      speed: map['speed']?.toDouble() ?? 0.0,
      speedAccuracy: map['speedAccuracy']?.toDouble() ?? 0.0,
      heading: map['heading']?.toDouble() ?? 0.0,
      time: map['time']?.toDouble() ?? 0.0,
    );
  }

  String toJson() => json.encode(toMap());

  factory GpsPoint.fromJson(String source) =>
      GpsPoint.fromMap(json.decode(source));

  @override
  String toString() {
    return 'latitude: $latitude, longitude: $longitude, accuracy: $accuracy, time: $time)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GpsPoint &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.accuracy == accuracy &&
        other.altitude == altitude &&
        other.speed == speed &&
        other.speedAccuracy == speedAccuracy &&
        other.heading == heading &&
        other.time == time;
  }

  @override
  int get hashCode {
    return latitude.hashCode ^
        longitude.hashCode ^
        accuracy.hashCode ^
        altitude.hashCode ^
        speed.hashCode ^
        speedAccuracy.hashCode ^
        heading.hashCode ^
        time.hashCode;
  }

  @override
  List<Object?> get props => throw UnimplementedError();
}
