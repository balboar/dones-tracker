part of 'location_tracking_cubit.dart';

abstract class LocationTrackingCubitState extends Equatable {
  const LocationTrackingCubitState();
  @override
  List<Object?> get props => [];
}

class LocationTrackingCubitInitial extends LocationTrackingCubitState {
  const LocationTrackingCubitInitial();
}

class LocationTrackingCubitStarting extends LocationTrackingCubitState {
  const LocationTrackingCubitStarting();
}

class LocationTrackingCubitTracking extends LocationTrackingCubitState {
  const LocationTrackingCubitTracking();
}

class LocationTrackingCubitLocationAdded extends LocationTrackingCubitState {
  final LatLng location;

  const LocationTrackingCubitLocationAdded(this.location);
  @override
  List<Object?> get props => [location];
}

class LocationTrackingCubitStopping extends LocationTrackingCubitState {
  const LocationTrackingCubitStopping();
}

class LocationTrackingCubitError extends LocationTrackingCubitState {
  final String message;

  const LocationTrackingCubitError(this.message);

  @override
  List<Object?> get props => [message];
}
