import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:latlong2/latlong.dart';
import 'package:seguidor/features/main_page/repositories/location_tracking_repo.dart';
import 'package:seguidor/core/constants.dart';

part 'location_trackin_state.dart';

class LocationTrackingCubit extends Cubit<LocationTrackingCubitState> {
  final LocationTrackingRepo locationTrackingRepo;
  LocationTrackingCubit({required this.locationTrackingRepo})
      : super(const LocationTrackingCubitInitial());

  void startLocationTracking(Sport sport) async {
    emit(const LocationTrackingCubitStarting());
    debugPrint('starting');
    final failureOrRsponse = await locationTrackingRepo.startActivity(sport);

    failureOrRsponse.fold((l) {
      debugPrint(l.message);
      emit(LocationTrackingCubitError(l.message));
    }, (r) {
      StreamController<LatLng> positionStream =
          locationTrackingRepo.locationStream!;

      positionStream.stream.listen((position) {
        emit(LocationTrackingCubitLocationAdded(
            LatLng(position.latitude, position.longitude)));

        debugPrint('Location added');
      });

      emit(const LocationTrackingCubitTracking());
      debugPrint('started');
    });
  }

  void resetLocationTrackingStateToInitial() {
    emit(locationTrackingRepo.activity == null
        ? const LocationTrackingCubitInitial()
        : const LocationTrackingCubitTracking());
  }

  void stopLocationTracking() async {
    emit(const LocationTrackingCubitStopping());
    debugPrint('stopping');

    final failureOrRsponse = await locationTrackingRepo.stopActivity();
    failureOrRsponse.fold(
      (l) {
        emit(LocationTrackingCubitError(l.message));
      },
      (r) {
        emit(const LocationTrackingCubitInitial());
        debugPrint('initial');
      },
    );
  }
}
