import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'package:battery_plus/battery_plus.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:gpx/gpx.dart';
import 'package:latlong2/latlong.dart';
import 'package:seguidor/core/notification.dart';
import 'package:seguidor/features/main_page/entities/activity_entity.dart';
import 'package:seguidor/features/main_page/entities/gps_point_entity.dart';
import 'package:seguidor/core/constants.dart';
import 'package:seguidor/features/settings_page/cubit/location_settings_cubit.dart';
import 'package:seguidor/features/sync/datasource/remote_api/api.dart';
import 'package:seguidor/features/sync/datasource/remote_api/api_exceptions.dart';
import 'package:seguidor/features/sync/datasource/remote_api/api_response.dart';
import 'package:seguidor/features/sync/failures/sync_failure.dart';
import 'package:path/path.dart' as p;
import 'package:seguidor/injection.dart';

class LocationTrackingRepo {
  LocationTrackingRepo();
  final RemoteApi _remoteApi = RemoteApi();

  final _battery = Battery();
  Activity? get activity => _activity;

  StreamController<LatLng>? _locationStream;
  StreamController<LatLng>? get locationStream => _locationStream;
  final Queue<GpsPoint> _notSyncedPositions = Queue();

  StreamSubscription<Position>? _positionStreamSubscription;
  final GeolocatorPlatform _geolocatorPlatform = GeolocatorPlatform.instance;
  final locationSettingsCubit = sl<LocationSettingsCubit>();

  Activity? _activity;

  void _startLocationTracking() {
    if (_positionStreamSubscription == null) {
      final currentSettings = locationSettingsCubit.state;
      final LocationSettings locationSettings = AndroidSettings(
          accuracy: currentSettings.accuracy,
          distanceFilter: currentSettings.distanceFilter,
          intervalDuration: currentSettings.intervalDuration,
          foregroundNotificationConfig: const ForegroundNotificationConfig(
            notificationText:
                "Seguidor will continue to receive your location even when you aren't using it",
            notificationTitle: "Dones en segundo plano",
            enableWakeLock: true,
          ));
      final positionStream = _geolocatorPlatform.getPositionStream(
          locationSettings: locationSettings);
      _locationStream ??= StreamController<LatLng>();
      _positionStreamSubscription = positionStream.handleError((error) {
        _cancelStremSubscription();
      }).listen(
        (position) {
          _locationStream!.add(LatLng(position.latitude, position.longitude));
          addNewPosition(position);
        },
      );
    }
  }

  void addNewPosition(Position position) async {
    int batteryLevel = await _battery.batteryLevel;

    _activity?.addPosition(
      GpsPoint.fromLocationData(position)..battery = batteryLevel,
    );
  }

  Future<Either<Failure, ApiResponse?>> syncData() async {
    if (_activity != null) {
      _notSyncedPositions.clear();
      _notSyncedPositions.addAll(
          _activity!.gpsPoints.where((element) => element.sync == false));
      if (_notSyncedPositions.isNotEmpty) {
        try {
          final response = await _remoteApi.postPosicion(_notSyncedPositions
              .map((GpsPoint position) => position.toMap())
              .toList());
          for (var position in _notSyncedPositions) {
            _activity!.gpsPoints
                .where((GpsPoint element) => element.time == position.time)
                .forEach((element) {
              element.sync = true;
            });
          }
          debugPrint(_notSyncedPositions.length.toString());

          return right(response);
        } on FetchDataException catch (e) {
          return left(ServerFailure(message: e.toString()));
        } catch (e) {
          return left(GeneralFailure(message: e.toString()));
        }
      }
    } else {
      return right(null);
    }
    return right(null);
  }

  Future<Either<Failure, ApiResponse?>> loadFileFromFileSystem() async {
    if (_activity != null) {
      String s;
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.any,
        allowMultiple: false,
      );
      if (result != null) {
        File file = File(result.files.single.path!);
        final basename = p.basename(file.path);

        try {
          s = await file.readAsString();
          GpxReader().fromString(s);
          return await _uploadFile(file.path, basename);
        } catch (e) {
          return left(GeneralFailure(message: e.toString()));
        }
      } else {
        return left(const GeneralFailure(
            message: 'No se ha seleccionado ningun fichero'));
      }
    } else {
      return left(
          const GeneralFailure(message: 'No hay ninguna actividad empezada'));
    }
  }

  void sendNotification(String title, String body) {
    if (kReleaseMode) {
      FirebaseFirestore.instance.collection('clientes').get().then((value) {
        for (var element in value.docs) {
          sendPushMessage(
              recipientToken: element.get('token'), title: title, body: body);
        }
      });
    }
  }

  Future<Either<Failure, ApiResponse?>> startActivity(Sport sport) async {
    _notSyncedPositions.clear();
    _activity = Activity(sport: sport);
    try {
      final response = await _remoteApi.postRuta(_activity!.toMap());
      _startLocationTracking();
      sendNotification('${sport.icon} Inicio actividad', sport.label);
      return right(response);
    } on FetchDataException catch (e) {
      _activity = null;
      return left(ServerFailure(message: e.toString()));
    } catch (e) {
      _activity = null;
      return left(GeneralFailure(message: e.toString()));
    }
  }

  Future<Either<Failure, ApiResponse?>> stopActivity() async {
    _activity!.stop();
    try {
      final response = await _remoteApi.putRuta(_activity!.toMap());
      _cancelStremSubscription();
      sendNotification('Fin actividad', '🛀 😈');
      return right(response);
    } on FetchDataException catch (e) {
      return left(ServerFailure(message: e.toString()));
    } catch (e) {
      return left(GeneralFailure(message: e.toString()));
    }
  }

  Future<Either<Failure, ApiResponse?>> _uploadFile(
      String file, String basename) async {
    try {
      var response = await _remoteApi.postFile(file, basename);
      _activity!.gpxRoute = basename;

      response = await _remoteApi.putRuta(_activity!.toMap());
      return right(response);
    } on FetchDataException catch (e) {
      return left(ServerFailure(message: e.toString()));
    } catch (e) {
      return left(GeneralFailure(message: e.toString()));
    }
  }

  void _cancelStremSubscription() {
    _positionStreamSubscription?.cancel();

    _positionStreamSubscription = null;
    if (_locationStream != null) {
      _locationStream!.close();
      _locationStream = null;
    }
  }
}
