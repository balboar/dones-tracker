import 'package:flutter/material.dart';

class StopDialogConfirmation extends StatelessWidget {
  const StopDialogConfirmation({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('¿Finalizar actividad?'),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.pop(context, true);
            },
            child: const Text('Finalizar')),
        TextButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            child: const Text('Cancelar')),
      ],
    );
  }
}
