import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:seguidor/core/location_permission_handler.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';
import 'package:seguidor/features/main_page/repositories/location_tracking_repo.dart';
import 'package:seguidor/injection.dart';

class MapWidget extends StatefulWidget {
  const MapWidget({super.key});

  @override
  State<MapWidget> createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  Marker? _lastPositionMarker;
  final MapController _mapController = MapController();
  @override
  void dispose() {
    _mapController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    if (!Platform.isLinux) {
      _getInitialGpsPosition().then((value) {
        if (value != null) {
          setState(() {
            _lastPositionMarker = Marker(
                point: LatLng(value.latitude, value.longitude),
                child: const PositionMarker());
          });
          _mapController.moveAndRotate(
              LatLng(value.latitude, value.longitude), 17, value.heading);
        }
      });
    }
    super.initState();
  }

  Future<Position?> _getInitialGpsPosition() async {
    bool locationPermissionGranted;
    locationPermissionGranted = await isPermissionLocationsGranted(context);

    if (locationPermissionGranted) {
      return Geolocator.getCurrentPosition();
    } else {
      return Future.value(null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationTrackingCubit, LocationTrackingCubitState>(
        builder: (context, state) {
      if (state is LocationTrackingCubitLocationAdded) {
        _lastPositionMarker =
            Marker(point: state.location, child: const PositionMarker());
      } else if (state is LocationTrackingCubitInitial) {}

      return FlutterMap(
        mapController: _mapController,
        options: const MapOptions(
          initialCenter: LatLng(50, 0),
          initialZoom: 4,
        ),
        children: [
          TileLayer(
            urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
          ),
          PolylineLayer(
            polylines: [
              Polyline(
                color: Theme.of(context).colorScheme.secondary,
                strokeWidth: 5,
                points: sl<LocationTrackingRepo>().activity != null
                    ? sl<LocationTrackingRepo>().activity!.positions
                    : [],
              ),
            ],
          ),
          MarkerLayer(
            rotate: true,
            markers: _lastPositionMarker != null ? [_lastPositionMarker!] : [],
          ),
          Align(
              alignment: Alignment.bottomRight,
              child: MapOptionButtons(mapController: _mapController)),
        ],
      );
    });
  }
}

class PositionMarker extends StatelessWidget {
  const PositionMarker({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      child: Icon(
        Icons.navigation_rounded,
        color: Theme.of(context).colorScheme.primary,
      ),
    );
  }
}

class MapOptionButtons extends StatelessWidget {
  const MapOptionButtons({
    super.key,
    required MapController mapController,
  }) : _mapController = mapController;

  final MapController _mapController;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).size.height / 5, right: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FloatingButtonZoomIn(mapController: _mapController),
          FloatingButtonZoomOut(mapController: _mapController),
          FloatingButtonCenterOnLocation(mapController: _mapController)
        ],
      ),
    );
  }
}

class FloatingButtonCenterOnLocation extends StatelessWidget {
  const FloatingButtonCenterOnLocation({
    super.key,
    required MapController mapController,
  }) : _mapController = mapController;

  final MapController _mapController;

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: sl<LocationTrackingRepo>().activity != null &&
          sl<LocationTrackingRepo>().activity!.gpsPoints.isNotEmpty,
      maintainSize: true,
      maintainAnimation: true,
      maintainState: true,
      child: FloatingActionButton.small(
          heroTag: 'center',
          child: const Icon(Icons.location_searching_rounded),
          onPressed: () {
            _mapController.moveAndRotate(
                LatLng(
                    sl<LocationTrackingRepo>()
                        .activity!
                        .gpsPoints
                        .last
                        .latitude,
                    sl<LocationTrackingRepo>()
                        .activity!
                        .gpsPoints
                        .last
                        .longitude),
                17,
                sl<LocationTrackingRepo>().activity!.gpsPoints.last.heading);
          }),
    );
  }
}

class FloatingButtonZoomOut extends StatelessWidget {
  const FloatingButtonZoomOut({
    super.key,
    required MapController mapController,
  }) : _mapController = mapController;

  final MapController _mapController;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.small(
        heroTag: 'zoomOut',
        child: const Icon(Icons.remove_outlined),
        onPressed: () {
          _mapController.move(
              _mapController.camera.center, _mapController.camera.zoom - 1);
        });
  }
}

class FloatingButtonZoomIn extends StatelessWidget {
  const FloatingButtonZoomIn({
    super.key,
    required MapController mapController,
  }) : _mapController = mapController;

  final MapController _mapController;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.small(
        heroTag: 'zoomIn',
        child: const Icon(Icons.add_rounded),
        onPressed: () {
          _mapController.move(
              _mapController.camera.center, _mapController.camera.zoom + 1);
        });
  }
}
