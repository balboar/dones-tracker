import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class EnableLocationDialog extends StatelessWidget {
  const EnableLocationDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      icon: const Icon(Icons.location_disabled_rounded),
      title: const Text('Ubicación desactivada'),
      content: const Text(
          'Tienes que activar la ubicación para que la aplicación funcione correctamente'),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            child: const Text('Cerrar')),
        TextButton(
            onPressed: () {
              Geolocator.openLocationSettings();
              Navigator.pop(context, true);
            },
            child: const Text('Abrir ajustes')),
      ],
    );
  }
}
