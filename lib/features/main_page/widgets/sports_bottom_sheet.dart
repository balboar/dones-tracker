import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';
import 'package:seguidor/core/constants.dart';

class ChooseSportBottomSheet extends StatelessWidget {
  const ChooseSportBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(15), child: const SportSwitcher());
  }
}

class SportSwitcher extends StatelessWidget {
  const SportSwitcher({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Center(
            child: Text(
              'Seleciona un deporte',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: Wrap(
            alignment: WrapAlignment.spaceEvenly,
            spacing: 10,
            runSpacing: 10,
            children: List.generate(
              Sport.values.length,
              (i) {
                return GestureDetector(onTap: () {
                  BlocProvider.of<LocationTrackingCubit>(context)
                      .startLocationTracking(Sport.values[i]);
                  Navigator.pop(context, true);
                }, child: LayoutBuilder(
                  builder: (context, constraints) {
                    return Container(
                        height: 100,
                        width: constraints.maxWidth / 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              width: 2, color: Theme.of(context).primaryColor),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Image(
                                width: 60,
                                height: 60,
                                image: AssetImage(Sport.values[i].imagePath),
                              ),
                              Center(
                                child: Text(
                                  Sport.values[i].label,
                                  style: Theme.of(context).textTheme.titleSmall,
                                ),
                              ),
                            ],
                          ),
                        ));
                  },
                ));
              },
            ),
          ),
        ),
      ],
    );
  }
}
