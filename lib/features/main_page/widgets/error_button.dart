import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';

class ErrorButton extends StatelessWidget {
  const ErrorButton({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: () {},
      child: FloatingActionButton.extended(
          onPressed: () {
            BlocProvider.of<LocationTrackingCubit>(context)
                .resetLocationTrackingStateToInitial();
          },
          backgroundColor: Colors.red,
          label: const Text('Error'),
          icon: const Icon(Icons.error_outlined)),
    );
  }
}
