import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';
import 'package:seguidor/features/main_page/widgets/error_button.dart';
import 'package:seguidor/features/main_page/widgets/start_button.dart';
import 'package:seguidor/features/main_page/widgets/stop_button.dart';

class LocationFloatingButton extends StatefulWidget {
  const LocationFloatingButton({super.key});

  @override
  State<LocationFloatingButton> createState() => LocationFloatingButtonState();
}

class LocationFloatingButtonState extends State<LocationFloatingButton>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 55,
      width: 100,
      child: BlocConsumer<LocationTrackingCubit, LocationTrackingCubitState>(
        listener: (context, state) {
          if (state is LocationTrackingCubitError) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                backgroundColor: Colors.red,
                content: Text(state.message),
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is LocationTrackingCubitInitial) {
            _animationController.reverse();
            return StartButton(animationController: _animationController);
          } else if ((state is LocationTrackingCubitStarting) ||
              (state is LocationTrackingCubitStopping)) {
            return FloatingActionButton(
                onPressed: () {},
                backgroundColor: null,
                child: const CircularProgressIndicator());
          } else if ((state is LocationTrackingCubitTracking) ||
              (state is LocationTrackingCubitLocationAdded)) {
            _animationController.forward();
            return StopButton(animationController: _animationController);
          } else if (state is LocationTrackingCubitError) {
            return const ErrorButton();
          } else {
            return StartButton(animationController: _animationController);
          }
        },
      ),
    );
  }
}
