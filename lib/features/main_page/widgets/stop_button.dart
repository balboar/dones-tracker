import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';
import 'package:seguidor/features/main_page/widgets/stop_route_dialog.dart';

class StopButton extends StatelessWidget {
  final AnimationController animationController;
  const StopButton({super.key, required this.animationController});

  Future<bool?> showStopConfirmationDialog(context) {
    return showDialog<bool>(
        context: context, builder: (_) => const StopDialogConfirmation());
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: () {},
      child: FloatingActionButton.extended(
          onPressed: () {
            showStopConfirmationDialog(context).then((value) {
              if ((value != null) && value) {
                BlocProvider.of<LocationTrackingCubit>(context)
                    .stopLocationTracking();
              }
            });
          },
          backgroundColor: null,
          label: const Text('Parar'),
          icon: AnimatedIcon(
            icon: AnimatedIcons.play_pause,
            progress: animationController,
          )),
    );
  }
}
