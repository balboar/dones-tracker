import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class RequestLocationPermissionDialog extends StatelessWidget {
  const RequestLocationPermissionDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      icon: const Icon(Icons.location_disabled_rounded),
      title: const Text('Permite acceder a la ubicación'),
      content: const Text(
          'Para que la aplicación funcione correctamente, necesitamos acceso a tu ubicación todo el tiempo, incluyendo cuando la aplicación está en segundo plano y acceso a la ubicación precisa.'),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            child: const Text('Ahora no')),
        TextButton(
            onPressed: () async {
              await Geolocator.openAppSettings()
                  .then((value) => Navigator.pop(context, true));
            },
            child: const Text('Abrir ajustes')),
      ],
    );
  }
}
