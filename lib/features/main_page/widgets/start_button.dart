import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:seguidor/core/location_permission_handler.dart';
import 'package:seguidor/features/main_page/widgets/enable_location_dialog.dart';
import 'package:seguidor/features/main_page/widgets/request_location_permission_dialog.dart';
import 'package:seguidor/features/main_page/widgets/sports_bottom_sheet.dart';

class StartButton extends StatelessWidget {
  final AnimationController animationController;
  const StartButton({super.key, required this.animationController});

  Future<void> showSportsBottomSheet(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        builder: (ctx) {
          return const ChooseSportBottomSheet();
        });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: () {},
      child: FloatingActionButton.extended(
          onPressed: () {
            isPermissionLocationsAlwaysGranted(context)
                .then((locationPermissionGranted) {
              if (!locationPermissionGranted) {
                showDialog(
                    context: context,
                    builder: (_) => const RequestLocationPermissionDialog());
              } else {
                Geolocator.isLocationServiceEnabled().then((gpsEnabled) {
                  if (gpsEnabled) {
                    showSportsBottomSheet(context);
                  } else {
                    showDialog(
                        context: context,
                        builder: (_) => const EnableLocationDialog());
                  }
                });
              }
            });
          },
          backgroundColor: null,
          label: const Text('Iniciar'),
          icon: AnimatedIcon(
            icon: AnimatedIcons.play_pause,
            progress: animationController,
          )),
    );
  }
}
