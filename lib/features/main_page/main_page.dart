import 'package:flutter/material.dart';
import 'package:seguidor/features/main_page/widgets/floating_button.dart';
import 'package:seguidor/features/upload_track_file/load_track_button.dart';
import 'package:seguidor/features/main_page/widgets/settings_button.dart';
import 'package:seguidor/features/main_page/widgets/map_widget.dart';
import 'package:seguidor/features/sync/sync_button.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        const MapWidget(),
        Positioned(
          top: MediaQuery.of(context).viewPadding.top + 10,
          right: 0,
          child: Card(
            margin: EdgeInsets.zero,
            color: Theme.of(context).colorScheme.primaryContainer,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.horizontal(
                left: Radius.circular(12),
              ),
            ),
            child: const Padding(
              padding: EdgeInsets.only(right: 10),
              child: Row(
                children: [
                  SyncButton(),
                  LoadTrackFileButton(),
                  SettingsButton()
                ],
              ),
            ),
          ),
        ),
      ]),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: const LocationFloatingButton(),
    );
  }
}
