import 'package:geolocator/geolocator.dart';
import 'package:seguidor/features/settings_page/entities/location_settings_entity.dart';

class LocationPresets {
  static final LocationSettingsModel bestAccuracy = LocationSettingsModel(
    id: 'best_accuracy',
    accuracy: LocationAccuracy.best,
    distanceFilter: 0,
    intervalDuration: const Duration(seconds: 5),
  );

  static final LocationSettingsModel mediumAccuracy = LocationSettingsModel(
    id: 'medium_accuracy',
    accuracy: LocationAccuracy.medium,
    distanceFilter: 50,
    intervalDuration: const Duration(seconds: 10),
  );

  static final LocationSettingsModel batterySaver = LocationSettingsModel(
    id: 'battery_saver',
    accuracy: LocationAccuracy.low,
    distanceFilter: 100,
    intervalDuration: const Duration(seconds: 20),
  );
}
