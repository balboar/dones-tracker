import 'package:geolocator/geolocator.dart';

class LocationSettingsModel {
  final String id; // Unique identifier
  final LocationAccuracy accuracy;
  final int distanceFilter;
  final Duration intervalDuration;

  LocationSettingsModel({
    required this.id,
    required this.accuracy,
    required this.distanceFilter,
    required this.intervalDuration,
  });

  // Convert to JSON
  Map<String, dynamic> toJson() => {
        'accuracy': accuracy.index,
        'distanceFilter': distanceFilter,
        'intervalDuration': intervalDuration.inSeconds,
        'id': id
      };

  // Convert from JSON
  factory LocationSettingsModel.fromJson(Map<String, dynamic> json) {
    return LocationSettingsModel(
      accuracy: LocationAccuracy.values[json['accuracy']],
      distanceFilter: json['distanceFilter'],
      intervalDuration: Duration(seconds: json['intervalDuration']),
      id: json['id'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocationSettingsModel &&
          runtimeType == other.runtimeType &&
          id == other.id; // Compare IDs for equality

  @override
  int get hashCode => id.hashCode; // Hash based on ID
}
