import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/settings_page/entities/location_presets.dart';
import 'package:seguidor/features/settings_page/entities/location_settings_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocationSettingsCubit extends Cubit<LocationSettingsModel> {
  LocationSettingsCubit() : super(LocationPresets.bestAccuracy) {
    _loadSettings();
  }

  // Method to load settings from SharedPreferences
  Future<void> _loadSettings() async {
    final prefs = await SharedPreferences.getInstance();
    final jsonString = prefs.getString('location_settings');

    // If saved settings exist, load them; otherwise, use the default state
    if (jsonString != null) {
      final savedSettings =
          LocationSettingsModel.fromJson(json.decode(jsonString));
      emit(savedSettings);
    } else {
      updateSettings(LocationPresets.mediumAccuracy);
    }
  }

  // Update settings and save to SharedPreferences
  Future<void> updateSettings(LocationSettingsModel newSettings) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(
        'location_settings', json.encode(newSettings.toJson()));
    emit(newSettings);
  }
}
