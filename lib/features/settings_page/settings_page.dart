import 'package:flutter/material.dart';
import 'package:seguidor/features/settings_page/cubit/location_settings_cubit.dart';
import 'package:seguidor/features/settings_page/entities/location_presets.dart';
import 'package:seguidor/features/settings_page/entities/location_settings_entity.dart';
import 'package:seguidor/injection.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final LocationSettingsCubit _locationSettingsCubit =
      sl<LocationSettingsCubit>();
  LocationSettingsModel?
      currentSettings; // Nullable to prevent initialization errors

  @override
  void initState() {
    super.initState();
    loadSettings(); // Load settings on startup
  }

  Future<void> loadSettings() async {
    currentSettings = _locationSettingsCubit.state;
  }

  @override
  Widget build(BuildContext context) {
    if (currentSettings == null) {
      return const Center(
          child:
              CircularProgressIndicator()); // Show loading indicator while settings are loaded
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Ajustes'), // Updated title to "Ajustes"
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Perfiles de Ubicación',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),

            const Divider(
              height: 20,
            ),

            // DropdownMenu for selecting preset profiles
            DropdownMenu<LocationSettingsModel>(
              initialSelection: currentSettings, // Set initial selection
              label: const Text('Seleccionar perfil'),
              onSelected: (LocationSettingsModel? setting) {
                setState(() {
                  currentSettings = setting; // Update current settings
                  _locationSettingsCubit.updateSettings(setting!);
                });
              },
              dropdownMenuEntries: [
                DropdownMenuEntry<LocationSettingsModel>(
                  value: LocationPresets.bestAccuracy,
                  label: 'Alta precisión',
                ),
                DropdownMenuEntry<LocationSettingsModel>(
                  value: LocationPresets.mediumAccuracy,
                  label: 'Precisión media',
                ),
                DropdownMenuEntry<LocationSettingsModel>(
                  value: LocationPresets.batterySaver,
                  label: 'Ahorro de batería',
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
