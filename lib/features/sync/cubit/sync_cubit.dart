import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/main_page/repositories/location_tracking_repo.dart';

part 'sync_state.dart';

class SyncCubit extends Cubit<SyncState> {
  final LocationTrackingRepo locationTrackingRepo;
  SyncCubit({required this.locationTrackingRepo}) : super(SyncInitial());

  void syncData() {
    bool canSync = true;
    Timer.periodic(const Duration(seconds: 5), (timer) async {
      if (canSync) {
        emit(SyncInProgress());
        canSync = false;
        final failureOrRsponse = await locationTrackingRepo.syncData();
        failureOrRsponse.fold(
          (l) => emit(SyncError(l.message)),
          (r) => emit(SyncInitial()),
        );
        canSync = true;
      }
    });
  }
}
