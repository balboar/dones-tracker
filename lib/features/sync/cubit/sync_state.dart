part of 'sync_cubit.dart';

sealed class SyncState extends Equatable {
  const SyncState();

  @override
  List<Object> get props => [];
}

final class SyncInitial extends SyncState {}

final class SyncInProgress extends SyncState {}

final class SyncError extends SyncState {
  final String message;

  const SyncError(this.message);
  @override
  List<Object> get props => [message];
}
