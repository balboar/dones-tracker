class ApiResponse<T> {
  T? data;

  String? message;

  ApiResponse(this.message);
}
