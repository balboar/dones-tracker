import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:seguidor/features/sync/datasource/remote_api/api_exceptions.dart';
import 'package:seguidor/features/sync/datasource/remote_api/api_response.dart';

String user = const String.fromEnvironment('user');
String pass = const String.fromEnvironment('password');
String url = const String.fromEnvironment('serverUrl');
String auth = 'Basic ${base64Encode(utf8.encode('$user:$pass'))}';
// ignore: constant_identifier_names
Map<String, String> _jsonHeaders = {
  "content-type": "application/json",
  'authorization': auth
};

class RemoteApi {
  Future<ApiResponse> postPosicion(List<Map<String, dynamic>> position) async {
    final String encodedData = json.encode(position);
    try {
      http.Response response = await http.post(Uri.parse('$url/position'),
          body: encodedData, headers: _jsonHeaders);
      return _returnResponse(response);
    } on SocketException catch (e) {
      throw FetchDataException(e.message);
    }
  }

  Future<ApiResponse> postRuta(Map<String, dynamic> ruta) async {
    final String encodedData = json.encode(ruta);
    try {
      http.Response response = await http.post(Uri.parse('$url/activity'),
          body: encodedData, headers: _jsonHeaders);
      return _returnResponse(response);
    } on SocketException catch (e) {
      throw FetchDataException(e.message);
    }
  }

  Future<ApiResponse> putRuta(Map<String, dynamic> ruta) async {
    final String encodedData = json.encode(ruta);
    try {
      http.Response response = await http.put(Uri.parse('$url/activity'),
          body: encodedData, headers: _jsonHeaders);
      return _returnResponse(response);
    } on SocketException catch (e) {
      throw FetchDataException(e.message);
    }
  }

  Future<ApiResponse> postFile(String file, String basename) async {
    try {
      var request =
          http.MultipartRequest('POST', Uri.parse('$url/track/$basename'));
      request.headers.addAll(
          {"content-type": "multipart/form-data", 'authorization': auth});
      request.files.add(await http.MultipartFile.fromPath('file', file));

      var res = await request.send();

      return _returnResponse(await http.Response.fromStream(res));
    } on SocketException catch (e) {
      throw FetchDataException(e.message);
    }
  }
}

ApiResponse _returnResponse(http.Response response) {
  switch (response.statusCode) {
    case 200:
      var responseJson = json.decode(response.body.toString());
      return ApiResponse(responseJson);
    case 201:
      var responseJson = json.decode(response.body.toString());
      return ApiResponse(responseJson);
    case 400:
      throw BadRequestException(response.body.toString());
    case 401:
    case 403:
      throw UnauthorisedException(response.body.toString());
    case 500:
    default:
      throw FetchDataException('${response.reasonPhrase} ${response.body}');
  }
}
