import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/core/info_dialog.dart';
import 'package:seguidor/features/sync/cubit/sync_cubit.dart';

class SyncButton extends StatefulWidget {
  const SyncButton({super.key});

  @override
  State<SyncButton> createState() => _SyncButtonState();
}

class _SyncButtonState extends State<SyncButton> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SyncCubit, SyncState>(
      builder: (context, state) {
        if (state is SyncError) {
          return IconButton(
              onPressed: () {
                showInfoDialog(context, state.message);
              },
              icon: const Icon(
                Icons.sync_problem_rounded,
                color: Colors.red,
              ));
        } else {
          return Container();
        }
      },
    );
  }
}
