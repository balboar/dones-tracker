part of 'upload_track_file_cubit.dart';

sealed class UploadTrackFileState extends Equatable {
  const UploadTrackFileState();

  @override
  List<Object> get props => [];
}

final class UploadTrackFileInitial extends UploadTrackFileState {}

final class UploadTrackFileSuccess extends UploadTrackFileState {}

final class UploadTrackFileError extends UploadTrackFileState {
  final String message;

  const UploadTrackFileError(this.message);
  @override
  List<Object> get props => [message];
}
