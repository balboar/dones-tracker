import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/main_page/repositories/location_tracking_repo.dart';

part 'upload_track_file_state.dart';

class UploadTrackFileCubit extends Cubit<UploadTrackFileState> {
  final LocationTrackingRepo locationTrackingRepo;
  UploadTrackFileCubit({required this.locationTrackingRepo})
      : super(UploadTrackFileInitial());

  void loadTrack() async {
    final failureOrRsponse =
        await locationTrackingRepo.loadFileFromFileSystem();
    failureOrRsponse.fold(
      (l) => emit(UploadTrackFileError(l.message)),
      (r) => emit(UploadTrackFileSuccess()),
    );
    emit(UploadTrackFileInitial());
  }
}
