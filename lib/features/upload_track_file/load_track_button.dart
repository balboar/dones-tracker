import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:seguidor/features/upload_track_file/cubit/upload_track_file_cubit.dart';

class LoadTrackFileButton extends StatelessWidget {
  const LoadTrackFileButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<UploadTrackFileCubit, UploadTrackFileState>(
      child: IconButton(
        onPressed: () {
          context.read<UploadTrackFileCubit>().loadTrack();
        },
        icon: const Icon(Icons.file_upload_outlined),
      ),
      listener: (context, state) {
        if (state is UploadTrackFileError) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.red,
              content: Text(state.message),
            ),
          );
        } else if (state is UploadTrackFileSuccess) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.green,
              content: Text('¡Ruta subida!'),
            ),
          );
        }
      },
    );
  }
}
