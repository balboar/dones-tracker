import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:seguidor/features/main_page/cubit/location_cubit/location_tracking_cubit.dart';
import 'package:seguidor/features/upload_track_file/cubit/upload_track_file_cubit.dart';
import 'package:seguidor/injection.dart';
import 'package:seguidor/features/main_page/main_page.dart';
import 'package:flutter/material.dart';
import 'package:seguidor/features/settings_page/settings_page.dart';
import 'package:seguidor/features/sync/cubit/sync_cubit.dart';
import 'package:dynamic_color/dynamic_color.dart';

final _router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      onExit: (context, state) {
        final providerState =
            BlocProvider.of<LocationTrackingCubit>(context, listen: false)
                .state;
        if (providerState is LocationTrackingCubitInitial) {
          return true;
        } else {
          return false;
        }
      },
      builder: (context, state) => const MainPage(),
    ),
    GoRoute(
      path: '/settings',
      builder: (context, state) => SettingsPage(),
    ),
  ],
);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent,
    ));
    return MultiBlocProvider(
        providers: [
          BlocProvider<LocationTrackingCubit>(
            create: (BuildContext context) => sl<LocationTrackingCubit>(),
          ),
          BlocProvider<UploadTrackFileCubit>(
            create: (BuildContext context) => sl<UploadTrackFileCubit>(),
          ),
          BlocProvider<SyncCubit>(
            lazy: false,
            create: (BuildContext context) =>
                SyncCubit(locationTrackingRepo: sl())..syncData(),
          ),
        ],
        child: DynamicColorBuilder(
            builder: (ColorScheme? lightDynamic, ColorScheme? darkDynamic) {
          return MaterialApp.router(
            theme: ThemeData(
              useMaterial3: true,
              colorScheme: lightDynamic ??
                  ColorScheme.fromSeed(seedColor: Colors.lightGreen),
            ),
            routerConfig: _router,
          );
        }));
  }
}
